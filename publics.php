<?php
/**
 * Created by PhpStorm.
 * User: user85
 * Date: 14/01/2018
 * Time: 09:47 PM
 */

function getDB()
{
    $connection = new mysqli("localhost", "root", "", "fridays");
    if ($connection->connect_error) {
        return null;
    } else {
        mysqli_query($connection, "set names utf8");//for save farsi language data,send all my query utf8
        return $connection;
    }
}

function getPDO()
{
    try {
        $DBH = new PDO("mysql:host=localhost;dbname=fridays", "root", "");
        $DBH->exec('set names utf8');
        return $DBH;
        // echo "connected";
    } catch (PDOException $myerror) {
        //echo "error connection";
        die("error in connectiong" . $myerror);
    }
}