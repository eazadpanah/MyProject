<!DOCTYPE html>
<!--This Layout designed byCSS 3.0 and HTML 5 IN PHPStorm-->
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User's List BY PDO </title>
    <link rel="stylesheet" href="css/styles.css" type="text/css">
</head>
<body>
<table id="MYLayout" align="center">
    <tr>
        <td id="MYHeader" colspan="2" bgcolor="#b0c4de">
            User's List BY PDO
        </td>
    </tr>
    <tr>
        <td id="MYSubHeader" colspan="2" bgcolor="#8fbc8f">
            This is SubHeader
        </td>
    </tr>
    <tr>
        <td id="MYMain">
            This is Main Content
            <br>
            <!--            HERE WE WANT TO SHOW USER'S LIST-->
            <table border="1" align="center" bgcolor="#ffc0cb">
                <?php

                require_once "publics.php";

                $connection = getPDO();
                //if we don't want to get sth from database , we use exec
                /*if we want to get sth from database we should use query,
                 but its not parametric , and if we want both we use execute*/
                $usersQuery = "SELECT * FROM users";
                $results = $connection->query($usersQuery);
                //here we have two dimentional array
                $users = $results->fetchAll(PDO::FETCH_ASSOC);
                //foreach is the best for peymayesh in array
                foreach ($users as $user) {
                    $username = $user['username'];
                    $password = $user['password'];
                    $name = $user['name'];
                    echo "<tr><td>" . $name . " " . $username . "</td></tr>";
                }
                /*                if (empty($connection->connect_error)) {
                                    $usersQuery = "SELECT * FROM users";
                                    $select = mysqli_query($connection, $usersQuery);
                                    if (mysqli_num_rows($select) > 0)//it means that we found users
                                    {
                                        while ($user = mysqli_fetch_assoc($select)) {
                                            $name = $user['name'];
                                            $username = $user['username'];
                                            $password = $user['password'];

                                            echo "<tr><td>" . $name . " " . $username . "</td></tr>";
                                        }
                                    }
                                }*/
                ?>
            </table>
        </td>
        <td id="MYMenu" bgcolor="#d3d3d3">
            <?php
            include "menu";
            ?>
        </td>
    </tr>
    <tr>
        <td id="MYSubFooter" colspan="2" bgcolor="#8fbc8f">
            This is SubFooter
        </td>
    </tr>
    <tr>
        <td id="MYFooter" colspan="2" bgcolor="#b0c4de">
            This is Footer: CopyRight © phptrainee.ir All Rights Reserved.
        </td>
    </tr>
</table>
</body>
</html>