<?php
/**
 * Created by PhpStorm.
 * User: user85
 * Date: 03/02/2018
 * Time: 07:32 PM
 */

include "converter.php";

function getShamsi()
{
    $datetime = getdate();
    $year = $datetime['year'];
    $month = $datetime['mon'];
    $day = $datetime['mday'];

    $shamsiArray = gregorian_to_jalali($year, $month, $day);
    $shamsiYear = $shamsiArray[0];
    $shamsiMonth = $shamsiArray[1];
    $shamsiDay = $shamsiArray[2];
    $tim=$datetime['hours'].":".$datetime['minutes'].":".$datetime['seconds'];
    $shamsi =$tim." ". $shamsiYear . "/" . $shamsiMonth . "/" . $shamsiDay;
    return $shamsi;
}

function logActions($action)
{
    $file = fopen("log", "a+");
    fwrite($file, getShamsi() . "\t" . $action."\n");
    fclose($file);
}

logActions("INSERT INTO users ....");
logActions("Update ....");
