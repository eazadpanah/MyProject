<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Send Posts</title>
    <link rel="stylesheet" href="css/styles.css" type="text/css">
    <script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
    
</head>
<body>
<table id="MYLayout" align="center">
    <tr>
        <td id="MYHeader" colspan="2" bgcolor="#b0c4de">
            <h3>send posts </h3>
        </td>
    </tr>
    <tr>
        <td id="MYSubHeader" colspan="2" bgcolor="#8fbc8f">
            This is SubHeader

        </td>
    </tr>
    <tr>
        <td id="MYMain">
            <form action="Destination.php" method="post">
                <input type="hidden" name="action" value="add_article">
                <input type="text" name="title" placeholder="enter title">
                <br><br>
                <textarea name="article" id="article"></textarea>
                <br><br>
                <script type="text/javascript">
                    CKEDITOR.replace('article', {
                        width: '600px',
                        height: '200px',
                    });
                </script>
                <input type="submit" value="save">
            </form>
        </td>
        <td id="MYMenu" bgcolor="#d3d3d3">
            <?php
            include "menu";
            ?>
        </td>
    </tr>
    <tr>
        <td id="MYSubFooter" colspan="2" bgcolor="#8fbc8f">
            This is SubFooter
        </td>
    </tr>
    <tr>
        <td id="MYFooter" colspan="2" bgcolor="#b0c4de">
            This is Footer: CopyRight © phptrainee.ir All Rights Reserved.
        </td>
    </tr>
</table>
</body>
</html>

