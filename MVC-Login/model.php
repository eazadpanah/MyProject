<?php
/**
 * Created by PhpStorm.
 * User: user85
 * Date: 06/02/2018
 * Time: 01:19 PM
 */


session_start();
function getDataBasePDO()
{
    $conn = new PDO("mysql:host=localhost;dbname=fridays", "root", "");
    $conn->exec('set names utf8');
    return $conn;
}
//------------------------------------------
function getUserId($username, $password)
{
    $connection = getDataBasePDO();
    $query = "SELECT * FROM users WHERE username=:username AND password=:password";
    $prepare = $connection->prepare($query);
    $bind = array("username" => $username, "password" => $password);
    $prepare->execute($bind);
    $users = $prepare->fetchAll(PDO::FETCH_ASSOC);
    if ($prepare->rowCount()> 0) {
        return $users[0]['id'];
    } else {
        return 0;
    }
}
//test model.php
if (getUserId("user123", "123") == true)
    echo "true";
else
    echo "false";
//------------------------------------------
function setUserIdToSession($userID)
{
    $_SESSION['userid'] = $userID;
}
