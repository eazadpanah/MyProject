<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Update Student</title>
    <link rel="stylesheet" href="css/styles.css" type="text/css">
</head>
<body>
<table id="MYLayout" align="center">
    <tr>
        <td id="MYHeader" colspan="2" bgcolor="#b0c4de">
            Login
        </td>
    </tr>
    <tr>
        <td id="MYSubHeader" colspan="2" bgcolor="#8fbc8f">
            This is SubHeader
        </td>
    </tr>
    <tr>
        <td id="MYMain">
            This is Main Content
            <br>

<?php
include "oop_register.php";
$std = new StudentManager(getDB());
$students = $std->getStudents();
echo "<form action=? method='GET' >
        <select name='student' > ";


foreach ($students as $student) {
    if ($_REQUEST['student'] == $student['id'])
        $selectd = 'selected';
    else
        $selectd = '';

    echo "<option  " . $selectd . "  value='" . $student['id'] . "'>" . $student['name'] . "</option>";
}

echo "</select>
<input type='submit' name='edit' value='Edit'>
    </form>";
?>
<br><br>

<?php
if (!empty($_REQUEST['student'])) {
    $id = $_REQUEST['student'];
    $stdManager = new StudentManager(getDB());
    $student = $stdManager->getStudentByID($id);


    ?>

    <form method="post" action="oop_register.php">
        <input type="hidden" name="action" value="edit">
        <input type="hidden" name="id" value="<?= $student['id'] ?>">
        <table border="1">
            <tr>
                <td>
                    <input type="text" name="name" value="<?= $student['name'] ?>" placeholder="Enter name">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="text" name="family" value="<?= $student['family'] ?>" placeholder="Enter family">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="number" name="student_number" value="<?= $student['student_number'] ?>"
                           placeholder="Enter Student number">
                </td>
            </tr>
            <tr>
                <td>
                    <select name="city">
                        <option value="1">Tehran</option>
                        <option value="2">Ahwaz</option>
                        <option value="3">Mashhad</option>
                        <option value="4">Tabriz</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="submit" value="Save">
                </td>
            </tr>


        </table>
    </form>

    <?php

}

?>
        </td>
        <td id="MYMenu" bgcolor="#d3d3d3">
            <ul>
                <li><a href="Index.php">Home</a></li>
                <li><a href="Arrays.php">Arrays</a></li>
                <li><a href="Login.php">Login</a></li>
                <li><a href="Movie.php">Movie</a></li>
                <li><a href="Weather.php">Weather</a></li>
                <li><a href="Send_article.php">SendPosts</a></li>
                <li><a href="Register.php">Register</a></li>
                <li><a href="Users.php">Users</a></li>
                <li><a href="../oop/register_student.html">OOP-Register-Student</a></li>
                <li><a href="../oop/update_student.php">OOP-Update-Student</a></li>
               <li><a href="Destination.php?action=Logoff" title="logoff">LogOff</a></li>
            </ul>
        </td>

    </tr>
    <tr>
        <td id="MYSubFooter" colspan="2" bgcolor="#8fbc8f">
            This is SubFooter
        </td>
    </tr>
    <tr>
        <td id="MYFooter" colspan="2" bgcolor="#b0c4de">
            This is Footer:  CopyRight © phptrainee.ir All Rights Reserved.
        </td>
    </tr>
</table>
</body>
</html>