<?php
/**
 * Created by PhpStorm.
 * User: amirhossein
 * Date: 1/19/18
 * Time: 11:04
 */


class Human
{
    public $name = "ali";
    public $family;
    public $nationality;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * @param mixed $family
     */
    private function setFamily($family)
    {
        $this->family = $family;
    }

    /**
     * @return mixed
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param mixed $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }


}

class Student extends Human
{


    public function getParentName(){
        return parent::getName() ;
    }

}
//
//$h = new Human();
//$h->setName("sdcsdcsd");
//
//
//$std = new Student();
//$std->setName("Mohammad");
//echo $std->getName() ;
//echo $std->getParentName() ;
//

//$std->setName("sdvsdv");
//