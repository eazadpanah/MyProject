<?php
/**
 * Created by PhpStorm.
 * User: amirhossein
 * Date: 1/19/18
 * Time: 09:41
 */

class Car
{
    private $name;
    private $year;
    private $color;
    private $owner;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }


    public function saveToDB()
    {
        $color = $this->getColor();
        $owner = $this->getOwner();
        $name = $this->getName();
        $insertQuery = "INSERT INTO cars (color,name,owner) 
          VALUES ('$color' , '$name' , '$owner') ";
        //execute query
    }


}


function newCar($name, $owner, $color)
{
    $samand = new Car();
    $samand->setName($name);
    $samand->setOwner($owner);
    $samand->setColor($color);
    $samand->saveToDB();
}


newCar("samand", "ali", "red");
newCar("samand lx", "alireza", "yellow");
newCar("samand lx", "alireza", "yellow");
newCar("samand lx", "alireza", "yellow");
newCar("samand lx", "alireza", "yellow");
newCar("samand lx", "alireza", "yellow");


class Student
{
    private $name ;

    public function __construct($name)
    {
        $this->name = $name;
    }


    public function getName()
    {
        return $this->name;
    }


}

$ali = new Student("Alireza") ;

echo $ali->getName() ;


echo "<Br><br><br>" ;



class Book{
    private $name ;
    private $author ;
    private $isbn ;
    private $year ;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getNameYear(){
        return "Name is : " . $this->getName() . " Year is : " . $this->getYear() ;
    }
    public function __destruct()
    {
        echo "<br>at the end of boofe koor<br>" ;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    public function getIsbn()
    {
        return $this->isbn;
    }

    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;
    }
    public function getYear()
    {
        return $this->year;
    }

    public function setYear($year)
    {
        $this->year = $year;
    }
}
$BoofeKoor = new Book("Boofe Koor") ;
$BoofeKoor->setYear(1300) ;
$BoofeKoor->setIsbn("213234324234") ;

echo $BoofeKoor->getNameYear() ;

echo $BoofeKoor->getIsbn() ;




class Foods{
    private $name ;
    private $price ;

    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function getPrice()
    {
        return $this->price;
    }
    public function setPrice($price)
    {
        $this->price = $price;
    }
    public function __destruct()
    {
        $price = $this->getPrice();
        $name  = $this->getName() ;
        $insertQuery = "INSERT INTO foods (name,price) 
      VALUES ('$name' , '$price') ";
        //execute query
    }
}

$burger = new Foods() ;
$burger->setName("Burger") ;
$burger->setPrice("123123") ;

$burger->setName("GHormesabzi") ;
