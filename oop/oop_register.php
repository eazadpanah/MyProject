<?php
/**
 * Created by PhpStorm.
 * User: amirhossein
 * Date: 1/19/18
 * Time: 11:23
 */
function getDB()
{
    $connection = new mysqli("localhost", "amir",
        "123", "fridays");
    if ($connection->connect_error) {
        return null;
    } else {
        mysqli_query($connection, "set names utf8");
        return $connection;
    }
}

class StudentManager
{
    private $name;
    private $family;
    private $city;
    private $student_number;
    private $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public static function getClass()
    {
        return "Sematec PHP Class";
    }

    public static function getDBType()
    {
        return "MySql";
    }

    public function register()
    {
        $name = $this->getName();
        $family = $this->getFamily();
        $city = $this->getCity();
        $stdNumber = $this->getStudentNumber();

        $insertQuery = "INSERT INTO students (name,family,student_number,city)
                VALUES ('$name' , '$family' , '$stdNumber' , '$city')";
        mysqli_query($this->connection, $insertQuery);
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getFamily()
    {
        return $this->family;
    }

    public function setFamily($family)
    {
        $this->family = $family;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function getStudentNumber()
    {
        return $this->student_number;
    }

    public function setStudentNumber($student_number)
    {
        $this->student_number = $student_number;
    }

    public function editStudent($id)
    {
        $name = $this->getName();
        $family = $this->getFamily();
        $city = $this->getCity();
        $stdNumber = $this->getStudentNumber();
        $editQuery = "UPDATE students SET name='$name' 
            , family='$family' , city='$city' , student_number='$stdNumber'
            WHERE id=$id
            ";
        mysqli_query($this->connection, $editQuery);

    }


    public function getStudents()
    {
        $result = mysqli_query($this->connection, "SELECT * from students");
        while ($row = mysqli_fetch_assoc($result)) {
            $students[] = $row;
        }
        return $students;
    }

    public function __destruct()
    {
        mysqli_close($this->connection);
    }

    public function getStudentByID($id)
    {
        $query = "SELECT * FROM students WHERE id=$id";
        $result = mysqli_query($this->connection, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            return $row;
        }
        return "";
    }

}


$action = $_REQUEST['action'];
if ($action == 'register') {
    $std = new StudentManager(getDB());
    $std->setName($_REQUEST['name']);
    $std->setFamily($_REQUEST['family']);
    $std->setCity($_REQUEST['city']);
    $std->setStudentNumber($_REQUEST['student_number']);
    $std->register();
}

if ($action == 'list') {
    $std = new StudentManager(getDB());
    $studentsArray = $std->getStudents();
    foreach ($studentsArray as $student) {
        echo $student['name'] . " " . $student['family'] . "<br>";
    }
}

if ($action == 'edit') {
    $std = new StudentManager(getDB());
    $std->setName($_REQUEST['name']);
    $std->setFamily($_REQUEST['family']);
    $std->setCity($_REQUEST['city']);
    $std->setStudentNumber($_REQUEST['student_number']);
    $std->editStudent($_REQUEST['id']);
}

//echo StudentManager::getClass() ;

abstract class  DbHandler
{
    abstract public function connect($server, $user, $pass);

    public function avoidInjection($query)
    {
        //check injection
    }
}

class StudentDbManage extends DbHandler
{


    public function connect($server, $user, $pass)
    {
        // TODO: Implement connect() method.
    }

    public function avoidInjection($query)
    {

    }
}


class DatabaseMG //singleton pattern
{
    private static $ins;

    public static function getIns()
    {
        if (DatabaseMG::$ins == null) {
            DatabaseMG::$ins = new DatabaseMG();
        }
        return DatabaseMG::$ins;
    }
}
$db= DatabaseMG::getIns();