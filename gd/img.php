<?php
/**
 * Created by PhpStorm.
 * User: user85
 * Date: 03/02/2018
 * Time: 05:47 PM
 */


header("Content-Type: image/png ");
session_start();

function randString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$img = imagecreate(200, 100);
$background_color = imagecolorallocate($img, 212, 255, 123);

$pixel_color = imagecolorallocate($img, 95, 84, 200);
for ($i = 0; $i <= 200; $i++) {
    imagesetpixel($img, rand(0, 200), rand(0, 100), $pixel_color);
}

$line_color = imagecolorallocate($img, 15, 200, 104);
for ($i = 0; $i <= 15; $i++) {
    imageline($img, rand(0, 50), rand(30, 50), rand(100, 150), rand(20, 85), $line_color);
}
$text_color = imagecolorallocate($img, 115, 100, 123);
$code = randString(5);
$_SESSION['code'] = $code;
imagestring($img, 30, 50, 35, $code, $text_color);

imagepng($img);
imagedestroy($img);